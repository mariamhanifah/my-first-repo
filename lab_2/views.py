from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers

def index(request):
    return render(request, "lab2.html")

def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")


def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")

