from django.contrib import admin
from django.urls import path, include
from lab_2.views import index as index_lab2
  
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index_lab2, name= 'index'),
    path('xml/', xml.site.urls),
    path('json/', json.site.urls),

]